﻿using System;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using IgdbApi.Models;
using IgdbApi.Search;
using Xunit;

namespace IgdbApi.Tests
{
    public class GamesApiTests
    {
        public abstract class GamesApiTestBase : TestBase
        {
            protected IIgdbGamesApi GamesApi { get; }

            protected GamesApiTestBase()
            {
                GamesApi = new IgdbApi("apiKey");
            }
        }

        public class GetGamesAsyncTests : GamesApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new {games = new GameSummary[0]});

                await GamesApi.GetGamesAsync();

                var call = HttpTest.CallLog[0];
                Assert.Equal("https://www.igdb.com/api/v1/games", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var games = new[] {new GameSummary {Id = 1}};
                HttpTest.RespondWithJson(200, new {games});

                var result = await GamesApi.GetGamesAsync(1);

                Assert.Equal(games, result, new PropertyEqualityComparer<GameSummary>());
            }

            [Fact]
            public async Task LimitParamSetsLimitQueryParam()
            {
                HttpTest.RespondWithJson(200, new {games = new GameSummary[0]});

                await GamesApi.GetGamesAsync(limit: 5);

                var call = HttpTest.CallLog[0];
                Assert.Equal("5", call.GetQueryParams()["limit"]);
            }

            [Fact]
            public async Task OffsetParamSetsOffsetQueryParam()
            {
                HttpTest.RespondWithJson(200, new {games = new GameSummary[0]});

                await GamesApi.GetGamesAsync(offset: 50);

                var call = HttpTest.CallLog[0];
                Assert.Equal("50", call.GetQueryParams()["offset"]);
            }

            [Fact]
            public async Task BothLimitAndOffsetAppearInQueryParam()
            {
                HttpTest.RespondWithJson(200, new {games = new GameSummary[0]});

                await GamesApi.GetGamesAsync(limit: 10, offset: 100);

                var call = HttpTest.CallLog[0];
                var queryParams = call.GetQueryParams();
                Assert.Equal("10", queryParams["limit"]);
                Assert.Equal("100", queryParams["offset"]);
            }
        }

        public class SearchGamesAsyncTests : GamesApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new {games = new GameSummary[0]});

                await GamesApi.SearchGamesAsync("test");

                var call = HttpTest.CallLog[0];
                Assert.Equal("https://www.igdb.com/api/v1/games/search?q=test", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var games = new[] {new GameSummary {Id = 1}};
                HttpTest.RespondWithJson(200, new {games});

                var result = await GamesApi.SearchGamesAsync("test");

                Assert.Equal(games, result, new PropertyEqualityComparer<GameSummary>());
            }

            [Fact]
            public async Task ThrowsIfQueryParamIsNullOrWhiteSpace()
            {
                await Assert.ThrowsAsync<ArgumentNullException>(() => GamesApi.SearchGamesAsync(null));
            }

            [Fact]
            public async Task CorrectlySetsFilterParameters()
            {
                var games = new[] {new GameSummary {Id = 1}};
                HttpTest.RespondWithJson(200, new {games});

                await GamesApi.SearchGamesAsync("test",
                    new GameSearchFilter
                    {
                        Field = GameFields.Keywords,
                        Operation = FilterOperation.InSet,
                        Value = "value"
                    });

                var call = HttpTest.CallLog[0];
                var queryParams = call.GetQueryParams();

                Assert.True(queryParams.ContainsKey("filters[keywords_in]"));
                Assert.Equal("value", queryParams["filters[keywords_in]"]);
            }
        }

        public class GetGameAsyncTests : GamesApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new {game = new Game()});

                await GamesApi.GetGameAsync(1);

                var call = HttpTest.CallLog[0];
                Assert.Equal("https://www.igdb.com/api/v1/games/1", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var game = new Game { Id = 1 };
                HttpTest.RespondWithJson(200, new {game});

                var result = await GamesApi.GetGameAsync(1);

                Assert.Equal(game, result, new PropertyEqualityComparer<Game>());
            }
        }

        public class GetGamesMetaInfoAsyncTests : GamesApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new GamesMetaInfo());

                await GamesApi.GetGamesMetaInfoAsync();

                var call = HttpTest.CallLog[0];
                Assert.Equal("https://www.igdb.com/api/v1/games/meta", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var metaInfo = new GamesMetaInfo {Size = 1};
                HttpTest.RespondWithJson(200, metaInfo);

                var result = await GamesApi.GetGamesMetaInfoAsync();

                Assert.Equal(metaInfo, result, new PropertyEqualityComparer<GamesMetaInfo>());
            }
        }
    }
}
