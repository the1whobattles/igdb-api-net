﻿using System.Threading.Tasks;
using IgdbApi.Models;
using Xunit;

namespace IgdbApi.Tests
{
    public class CompaniesApiTests
    {
        public abstract class CompaniesApiTestBase : TestBase
        {
            protected IIgdbCompaniesApi CompaniesApi { get; }

            protected CompaniesApiTestBase()
            {
                CompaniesApi = new IgdbApi("apiKey");
            }
        }

        public class GetCompaniesAsyncTests : CompaniesApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new {companies = new CompanySummary[0]});

                await CompaniesApi.GetCompaniesAsync();

                var call = HttpTest.CallLog[0];
                Assert.Equal("https://www.igdb.com/api/v1/companies", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var companies = new[] {new CompanySummary {Id = 1}};
                HttpTest.RespondWithJson(200, new {companies});

                var result = await CompaniesApi.GetCompaniesAsync();

                Assert.Equal(companies, result, new PropertyEqualityComparer<CompanySummary>());
            }

            [Fact]
            public async Task LimitParamSetsLimitQueryParam()
            {
                HttpTest.RespondWithJson(200, new {companies = new CompanySummary[0]});

                await CompaniesApi.GetCompaniesAsync(limit: 5);

                var call = HttpTest.CallLog[0];
                Assert.Equal("5", call.GetQueryParams()["limit"]);
            }

            [Fact]
            public async Task OffsetParamSetsOffsetQueryParam()
            {
                HttpTest.RespondWithJson(200, new {companies = new CompanySummary[0]});

                await CompaniesApi.GetCompaniesAsync(offset: 50);

                var call = HttpTest.CallLog[0];
                Assert.Equal("50", call.GetQueryParams()["offset"]);
            }

            [Fact]
            public async Task BothLimitAndOffsetAppearInQueryParam()
            {
                HttpTest.RespondWithJson(200, new {companies = new CompanySummary[0]});

                await CompaniesApi.GetCompaniesAsync(limit: 10, offset: 100);

                var call = HttpTest.CallLog[0];
                var queryParams = call.GetQueryParams();
                Assert.Equal("10", queryParams["limit"]);
                Assert.Equal("100", queryParams["offset"]);
            }
        }

        public class GetCompanyAsyncTests : CompaniesApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new {company = new Company()});

                await CompaniesApi.GetCompanyAsync(1);

                var call = HttpTest.CallLog[0];
                Assert.Equal("https://www.igdb.com/api/v1/companies/1", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var company = new Company { Id = 1 };
                HttpTest.RespondWithJson(200, new { company });

                var result = await CompaniesApi.GetCompanyAsync(1);

                Assert.Equal(company, result, new PropertyEqualityComparer<Company>());
            }
        }

        public class GetCompanyGamesAsyncTests : CompaniesApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new {games = new GameSummary[0]});

                await CompaniesApi.GetCompanyGamesAsync(1);

                var call = HttpTest.CallLog[0];
                Assert.Equal("https://www.igdb.com/api/v1/companies/1/games", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var games = new[] {new GameSummary {Id = 1}};
                HttpTest.RespondWithJson(200, new {games});

                var result = await CompaniesApi.GetCompanyGamesAsync(1);

                Assert.Equal(games, result, new PropertyEqualityComparer<GameSummary>());
            }

            [Fact]
            public async Task LimitParamSetsLimitQueryParam()
            {
                HttpTest.RespondWithJson(200, new {games = new GameSummary[0]});

                await CompaniesApi.GetCompanyGamesAsync(1, limit: 5);

                var call = HttpTest.CallLog[0];
                Assert.Equal("5", call.GetQueryParams()["limit"]);
            }

            [Fact]
            public async Task OffsetParamSetsOffsetQueryParam()
            {
                HttpTest.RespondWithJson(200, new {games = new GameSummary[0]});

                await CompaniesApi.GetCompanyGamesAsync(1, offset: 50);

                var call = HttpTest.CallLog[0];
                Assert.Equal("50", call.GetQueryParams()["offset"]);
            }

            [Fact]
            public async Task BothLimitAndOffsetAppearInQueryParam()
            {
                HttpTest.RespondWithJson(200, new {games = new GameSummary[0]});

                await CompaniesApi.GetCompanyGamesAsync(1, limit: 10, offset: 100);

                var call = HttpTest.CallLog[0];
                var queryParams = call.GetQueryParams();
                Assert.Equal("10", queryParams["limit"]);
                Assert.Equal("100", queryParams["offset"]);
            }
        }

        public class GetCompaniesMetaInfoAsyncTests : CompaniesApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new CompaniesMetaInfo());

                await CompaniesApi.GetCompaniesMetaInfoAsync();

                var call = HttpTest.CallLog[0];
                Assert.Equal("https://www.igdb.com/api/v1/companies/meta", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var metaInfo = new CompaniesMetaInfo {Size = 1};
                HttpTest.RespondWithJson(200, metaInfo);

                var result = await CompaniesApi.GetCompaniesMetaInfoAsync();

                Assert.Equal(metaInfo, result, new PropertyEqualityComparer<CompaniesMetaInfo>());
            }
        }
    }
}
