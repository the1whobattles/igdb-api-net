﻿using System.Linq;
using System.Threading.Tasks;
using IgdbApi.Models;
using Xunit;

namespace IgdbApi.Tests
{
    public class FranchisesApiTests
    {
        public abstract class FranchisesApiTestBase : TestBase
        {
            protected IIgdbFranchisesApi FranchisesApi { get; }

            protected FranchisesApiTestBase()
            {
                FranchisesApi = new IgdbApi("apiKey");
            }
        }

        public class GetFranchisesAsyncTests : FranchisesApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new {franchises = new FranchiseSummary[0]});

                await FranchisesApi.GetFranchisesAsync();


                var call = HttpTest.CallLog[0];

                Assert.NotNull(call);
                Assert.Equal("https://www.igdb.com/api/v1/franchises", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var franchises = new[] {new FranchiseSummary {Id = 1}};
                HttpTest.RespondWithJson(200, new {franchises});

                var result = await FranchisesApi.GetFranchisesAsync();

                Assert.Equal(franchises, result, new PropertyEqualityComparer<FranchiseSummary>());
            }

            [Fact]
            public async Task LimitParamSetsLimitQueryParam()
            {
                HttpTest.RespondWithJson(200, new {franchises = new FranchiseSummary[0]});

                await FranchisesApi.GetFranchisesAsync(limit: 5);


                var call = HttpTest.CallLog[0];

                Assert.NotNull(call);
                Assert.Equal("5", call.GetQueryParams()["limit"]);
            }

            [Fact]
            public async Task OffsetParamSetsOffsetQueryParam()
            {
                HttpTest.RespondWithJson(200, new {franchises = new FranchiseSummary[0]});

                await FranchisesApi.GetFranchisesAsync(offset: 50);


                var call = HttpTest.CallLog[0];

                Assert.NotNull(call);
                Assert.Equal("50", call.GetQueryParams()["offset"]);
            }

            [Fact]
            public async Task BothLimitAndOffsetAppearInQueryParam()
            {
                HttpTest.RespondWithJson(200, new {franchises = new FranchiseSummary[0]});

                await FranchisesApi.GetFranchisesAsync(limit: 10, offset: 100);


                var call = HttpTest.CallLog[0];

                Assert.NotNull(call);

                var queryParams = call.GetQueryParams();
                Assert.Equal("10", queryParams["limit"]);
                Assert.Equal("100", queryParams["offset"]);
            }
        }

        public class GetFranchiseAsyncTests : FranchisesApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new GamesMetaInfo());

                await FranchisesApi.GetFranchiseAsync(1);


                var call = HttpTest.CallLog[0];

                Assert.NotNull(call);
                Assert.Equal("https://www.igdb.com/api/v1/franchises/1", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var franchise = new Franchise {Id = 1};
                HttpTest.RespondWithJson(200, new {franchise});

                var result = await FranchisesApi.GetFranchiseAsync(1);

                Assert.Equal(franchise, result, new PropertyEqualityComparer<Franchise>());
            }
        }

        public class GetFranchiseGamesAsyncTests : FranchisesApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new GamesMetaInfo());

                await FranchisesApi.GetFranchiseGamesAsync(1);


                var call = HttpTest.CallLog[0];

                Assert.NotNull(call);
                Assert.Equal("https://www.igdb.com/api/v1/franchises/1/games", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var games = new[] {new GameSummary {Id = 1}};
                HttpTest.RespondWithJson(200, new {games});

                var result = await FranchisesApi.GetFranchiseGamesAsync(1);

                Assert.Equal(games, result, new PropertyEqualityComparer<GameSummary>());
            }

            [Fact]
            public async Task LimitParamSetsLimitQueryParam()
            {
                HttpTest.RespondWithJson(200, new {games = new GameSummary[0]});

                await FranchisesApi.GetFranchiseGamesAsync(1, limit: 5);


                var call = HttpTest.CallLog[0];

                Assert.NotNull(call);
                Assert.Equal("5", call.GetQueryParams()["limit"]);
            }

            [Fact]
            public async Task OffsetParamSetsOffsetQueryParam()
            {
                HttpTest.RespondWithJson(200, new {games = new GameSummary[0]});

                await FranchisesApi.GetFranchiseGamesAsync(1, offset: 50);


                var call = HttpTest.CallLog[0];

                Assert.NotNull(call);
                Assert.Equal("50", call.GetQueryParams()["offset"]);
            }

            [Fact]
            public async Task BothLimitAndOffsetAppearInQueryParam()
            {
                HttpTest.RespondWithJson(200, new {games = new GameSummary[0]});

                await FranchisesApi.GetFranchiseGamesAsync(1, limit: 10, offset: 100);


                var call = HttpTest.CallLog[0];

                Assert.NotNull(call);

                var queryParams = call.GetQueryParams();
                Assert.Equal("10", queryParams["limit"]);
                Assert.Equal("100", queryParams["offset"]);
            }
        }

        public class GetFranchisesMetaInfoAsyncTests : FranchisesApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new FranchisesMetaInfo());

                await FranchisesApi.GetFranchisesMetaInfoAsync();

                var call = HttpTest.CallLog[0];

                Assert.NotNull(call);
                Assert.Equal("https://www.igdb.com/api/v1/franchises/meta", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var metaInfo = new FranchisesMetaInfo {Size = 1};
                HttpTest.RespondWithJson(200, metaInfo);

                var result = await FranchisesApi.GetFranchisesMetaInfoAsync();

                Assert.Equal(metaInfo, result, new PropertyEqualityComparer<FranchisesMetaInfo>());
            }
        }
    }
}
