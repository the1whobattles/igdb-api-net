﻿using System;
using Flurl.Http.Testing;

namespace IgdbApi.Tests
{
    public abstract class TestBase : IDisposable
    {
        protected HttpTest HttpTest { get; }

        protected TestBase()
        {
            HttpTest = new HttpTest();
        }

        #region IDisposable Support
        private bool _disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    HttpTest.Dispose();
                }

                _disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}
