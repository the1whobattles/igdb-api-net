﻿using System.Linq;
using System.Threading.Tasks;
using IgdbApi.Models;
using Xunit;

namespace IgdbApi.Tests
{
    public class PlatformsApiTests
    {
        public abstract class PlatformsApiTestBase : TestBase
        {
            protected IIgdbPlatformsApi PlatformsApi { get; }

            protected PlatformsApiTestBase()
            {
                PlatformsApi = new IgdbApi("apiKey");
            }
        }

        public class GetPlatformsAsyncTests : PlatformsApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new {platforms = new PlatformSummary[0]});

                await PlatformsApi.GetPlatformsAsync();

                var call = HttpTest.CallLog[0];
                Assert.Equal("https://www.igdb.com/api/v1/platforms", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var platforms = new[] {new PlatformSummary { Id = 1}};
                HttpTest.RespondWithJson(200, new {platforms});

                var result = await PlatformsApi.GetPlatformsAsync();

                Assert.Equal(platforms, result, new PropertyEqualityComparer<PlatformSummary>());
            }

            [Fact]
            public async Task LimitParamSetsLimitQueryParam()
            {
                HttpTest.RespondWithJson(200, new {platforms = new PlatformSummary[0]});

                await PlatformsApi.GetPlatformsAsync(limit: 5);

                var call = HttpTest.CallLog[0];
                Assert.Equal("5", call.GetQueryParams()["limit"]);
            }

            [Fact]
            public async Task OffsetParamSetsOffsetQueryParam()
            {
                HttpTest.RespondWithJson(200, new {platforms = new PlatformSummary[0]});

                await PlatformsApi.GetPlatformsAsync(offset: 50);

                var call = HttpTest.CallLog[0];
                Assert.Equal("50", call.GetQueryParams()["offset"]);
            }

            [Fact]
            public async Task BothLimitAndOffsetAppearInQueryParam()
            {
                HttpTest.RespondWithJson(200, new {platforms = new PlatformSummary[0]});

                await PlatformsApi.GetPlatformsAsync(limit: 10, offset: 100);

                var call = HttpTest.CallLog[0];
                var queryParams = call.GetQueryParams();
                Assert.Equal("10", queryParams["limit"]);
                Assert.Equal("100", queryParams["offset"]);
            }
        }

        public class GetPlatformAsyncTests : PlatformsApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new {platform = new Platform()});

                await PlatformsApi.GetPlatformAsync(1);

                var call = HttpTest.CallLog[0];
                Assert.Equal("https://www.igdb.com/api/v1/platforms/1", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var platform = new Platform { Id = 1 };
                HttpTest.RespondWithJson(200, new {platform});

                var result = await PlatformsApi.GetPlatformAsync(1);

                Assert.Equal(platform, result, new PropertyEqualityComparer<Platform>());
            }
        }

        public class GetPlatformGamesAsyncTests : PlatformsApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new GamesMetaInfo());

                await PlatformsApi.GetPlatformGamesAsync(1);

                var call = HttpTest.CallLog[0];
                Assert.Equal("https://www.igdb.com/api/v1/platforms/1/games", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var games = new[] {new GameSummary {Id = 1}};
                HttpTest.RespondWithJson(200, new {games});

                var result = await PlatformsApi.GetPlatformGamesAsync(1);

                Assert.Equal(games, result, new PropertyEqualityComparer<GameSummary>());
            }

            [Fact]
            public async Task LimitParamSetsLimitQueryParam()
            {
                HttpTest.RespondWithJson(200, new {games = new GameSummary[0]});

                await PlatformsApi.GetPlatformGamesAsync(1, limit: 5);

                var call = HttpTest.CallLog[0];
                Assert.Equal("5", call.GetQueryParams()["limit"]);
            }

            [Fact]
            public async Task OffsetParamSetsOffsetQueryParam()
            {
                HttpTest.RespondWithJson(200, new {games = new GameSummary[0]});

                await PlatformsApi.GetPlatformGamesAsync(1, offset: 50);

                var call = HttpTest.CallLog[0];
                Assert.Equal("50", call.GetQueryParams()["offset"]);
            }

            [Fact]
            public async Task BothLimitAndOffsetAppearInQueryParam()
            {
                HttpTest.RespondWithJson(200, new {games = new GameSummary[0]});

                await PlatformsApi.GetPlatformGamesAsync(1, limit: 10, offset: 100);

                var call = HttpTest.CallLog[0];
                var queryParams = call.GetQueryParams();
                Assert.Equal("10", queryParams["limit"]);
                Assert.Equal("100", queryParams["offset"]);
            }
        }

        public class GetPlatformsMetaInfoAsyncTests : PlatformsApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new PlatformsMetaInfo());

                await PlatformsApi.GetPlatformsMetaInfoAsync();

                var call = HttpTest.CallLog[0];
                Assert.Equal("https://www.igdb.com/api/v1/platforms/meta", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var metaInfo = new PlatformsMetaInfo {Size = 1};
                HttpTest.RespondWithJson(200, metaInfo);

                var result = await PlatformsApi.GetPlatformsMetaInfoAsync();

                Assert.Equal(metaInfo, result, new PropertyEqualityComparer<PlatformsMetaInfo>());
            }
        }
    }
}
