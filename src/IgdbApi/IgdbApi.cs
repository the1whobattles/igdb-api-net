﻿using System;
using System.Diagnostics.Contracts;
using Flurl.Http;

namespace IgdbApi
{
    /// <summary>
    /// Provides an implementation for the IGDB API.
    /// </summary>
    public partial class IgdbApi : IIgdbApi
    {
        private const string BaseUrl = "https://www.igdb.com/api/v1";

        private FlurlClient Client => new FlurlClient()
            .WithHeader("Authorization", $"Token token=\"{ApiKey}\"");

        /// <summary>
        /// Gets the API key being used for requests to the IGDB API.
        /// </summary>
        public string ApiKey { get; }

        /// <summary>
        /// Creates a new instance of the <see cref="IgdbApi"/> class.
        /// </summary>
        /// <param name="apiKey">The API key to use for requests to the IGDB API.</param>
        public IgdbApi(string apiKey)
        {
            Contract.Requires<ArgumentNullException>(!String.IsNullOrWhiteSpace(apiKey), nameof(apiKey));

            ApiKey = apiKey;
        }
    }
}
