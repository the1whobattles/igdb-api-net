﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using IgdbApi.Models;
using IgdbApi.Models.Wrappers;

namespace IgdbApi
{
    public partial class IgdbApi : IIgdbPeopleApi
    {
        private static readonly string PeopleUrl = BaseUrl.AppendPathSegment("people");

        /// <summary>
        /// Retrieves a list of platforms as an asynchronous operation.
        /// </summary>
        /// <param name="limit">The limit of how many results should be returned.</param>
        /// <param name="offset">The offset into the list of games where results should be returned from.</param>
        /// <returns>A list of person summaries.</returns>
        /// <remarks>
        /// <para>If unset, <paramref name="limit"/> defaults to 25 results, which is the maximum allowed per call.</para>
        /// <para>If unset, <paramref name="offset"/> defaults to 0.</para>
        /// </remarks>
        public async Task<IEnumerable<PersonSummary>> GetPeopleAsync(int? limit, int? offset)
        {
            Url reqUrl = PeopleUrl;
            if (limit.HasValue)
                reqUrl.SetQueryParam("limit", limit.Value);
            if (offset.HasValue)
                reqUrl.SetQueryParam("offset", offset.Value);

            using (var client = Client)
            {
                var list = await client.WithUrl(reqUrl).GetJsonAsync<GetListWrapper>();
                return list?.People ?? Enumerable.Empty<PersonSummary>();
            }
        }

        /// <summary>
        /// Retrieve's a specific person's information as an asynchronous operation.
        /// </summary>
        /// <param name="personId">The ID of the person to look up.</param>
        /// <returns>The full information of the specified person.</returns>
        public async Task<Person> GetPersonAsync(int personId)
        {
            var reqUrl = PeopleUrl.AppendPathSegment(personId.ToString());

            using (var client = Client)
            {
                var wrapper = await client.WithUrl(reqUrl).GetJsonAsync<GetWrapper>();
                return wrapper?.Person;
            }
        }

        /// <summary>
        /// Retrieves a list of games related to a specified person as an asynchronous operation.
        /// </summary>
        /// <param name="personId">The ID of the person whose games are being looked up.</param>
        /// <param name="limit">The limit of how many results should be returned.</param>
        /// <param name="offset">The offset into the list of games where results should be returned from.</param>
        /// <returns>A list of game summaries.</returns>
        /// <remarks>
        /// <para>If unset, <paramref name="limit"/> defaults to 25 results, which is the maximum allowed per call.</para>
        /// <para>If unset, <paramref name="offset"/> defaults to 0.</para>
        /// </remarks>
        public async Task<IEnumerable<GameSummary>> GetPersonGamesAsync(int personId, int? limit = default(int?), int? offset = default(int?))
        {
            var reqUrl = PeopleUrl
                .AppendPathSegment(personId.ToString())
                .AppendPathSegment("games");
            if (limit.HasValue)
                reqUrl.SetQueryParam("limit", limit.Value);
            if (offset.HasValue)
                reqUrl.SetQueryParam("offset", offset.Value);

            using (var client = Client)
            {
                var list = await client.WithUrl(reqUrl).GetJsonAsync<GetListWrapper>();
                return list?.Games ?? Enumerable.Empty<GameSummary>();
            }
        }

        /// <summary>
        /// Retrieves information about the list of people known to IGDB.
        /// </summary>
        /// <returns>An object describing the meta information for the people collection.</returns>
        public Task<PeopleMetaInfo> GetPeopleMetaInfoAsync()
        {
            using (var client = Client)
            {
                var reqUrl = PeopleUrl.AppendPathSegment("meta");
                return client.WithUrl(reqUrl).GetJsonAsync<PeopleMetaInfo>();
            }
        }
    }
}