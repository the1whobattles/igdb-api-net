﻿using System;
using Newtonsoft.Json;

namespace IgdbApi.Models
{
    /// <summary>
    /// Represents basic details for a game.
    /// </summary>
    public class GameSummary : GameBase
    {

        /// <summary>
        /// Gets the first alternative name of the game.
        /// </summary>
        [JsonProperty("alternative_name")]
        public string AlternativeName { get; internal set; }

        /// <summary>
        /// Gets the first release date of the game.
        /// </summary>
        [JsonProperty("release_date")]
        public DateTime? ReleaseDate { get; internal set; }
    }
}
