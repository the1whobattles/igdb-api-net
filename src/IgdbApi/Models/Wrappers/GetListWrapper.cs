﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace IgdbApi.Models.Wrappers
{
    internal class GetListWrapper
    {
        [JsonProperty("games")]
        public List<GameSummary> Games { get; set; }

        [JsonProperty("companies")]
        public List<CompanySummary> Companies { get; set; }

        [JsonProperty("franchises")]
        public List<FranchiseSummary> Franchises { get; set; }

        [JsonProperty("platforms")]
        public List<PlatformSummary> Platforms { get; set; }

        [JsonProperty("people")]
        public List<PersonSummary> People { get; set; }
    }
}
