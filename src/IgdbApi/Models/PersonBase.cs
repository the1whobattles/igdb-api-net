﻿using Newtonsoft.Json;

namespace IgdbApi.Models
{
    /// <summary>
    /// Represents basic properties of people on IGDB.
    /// </summary>
    public class PersonBase
    {
        /// <summary>
        /// Gets the ID value of the person.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets the URL slug for the person's page on the IGDB website.
        /// </summary>
        [JsonProperty("slug")]
        public string Slug { get; internal set; }

        /// <summary>
        /// Gets the name of the person.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; internal set; }
    }
}
