﻿using System;
using Newtonsoft.Json;

namespace IgdbApi.Models
{
    /// <summary>
    /// Represents when a game was released on a specified platform.
    /// </summary>
    public class ReleaseDateInfo
    {
        /// <summary>
        /// Gets the name of the platform for the game's release.
        /// </summary>
        [JsonProperty("platform_name")]
        public string PlatformName { get; internal set; }

        /// <summary>
        /// Gets the date of the game's release on the specified platform.
        /// </summary>
        [JsonProperty("release_date")]
        public DateTime ReleaseDate { get; internal set; }
    }
}
