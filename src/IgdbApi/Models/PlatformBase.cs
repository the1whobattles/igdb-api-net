﻿using Newtonsoft.Json;

namespace IgdbApi.Models
{
    /// <summary>
    /// Represents the basic properties of platforms in IGDB.
    /// </summary>
    public class PlatformBase
    {
        /// <summary>
        /// Gets the ID value of the game.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets the URL slug for the game's page on the IGDB website.
        /// </summary>
        [JsonProperty("slug")]
        public string Slug { get; internal set; }

        /// <summary>
        /// Gets the name of the game.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; internal set; }
    }
}
