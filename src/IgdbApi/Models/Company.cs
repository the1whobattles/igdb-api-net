﻿using Newtonsoft.Json;

namespace IgdbApi.Models
{
    /// <summary>
    /// Represents a company.
    /// </summary>
    public class Company : CompanyBase
    {
        /// <summary>
        /// Gets the URL slug for the company's page on the IGDB website.
        /// </summary>
        [JsonProperty("slug")]
        public string Slug { get; internal set; }

        /// <summary>
        /// Gets the aggregate review rating of the the company's games.
        /// </summary>
        [JsonProperty("average_rating")]
        public double AverageRating { get; internal set; }

        /// <summary>
        /// Gets the ID of the company's parent organization.
        /// </summary>
        [JsonProperty("parent")]
        public int? ParentId { get; internal set; }

        /// <summary>
        /// Gets details about the company's logo as stored on IGDB.
        /// </summary>
        [JsonProperty("company_logo")]
        public ImageInfo CompanyLogo { get; internal set; }

        /// <summary>
        /// Gets the year the company was founded.
        /// </summary>
        [JsonProperty("founded_year")]
        public int? FoundedYear { get; internal set; }
    }
}
