﻿using System;
using Newtonsoft.Json;

namespace IgdbApi.Models
{
    /// <summary>
    /// Represents a person associated with the development or publication of a game.
    /// </summary>
    public class Person : PersonBase
    {
        /// <summary>
        /// Gets the person's date of birth.
        /// </summary>
        [JsonProperty("dob")]
        public DateTime? DateOfBirth { get; internal set; }

        /// <summary>
        /// Gets the ID of the person's country of residence.
        /// </summary>
        [JsonProperty("country")]
        public int? CountryId { get; internal set; }

        /// <summary>
        /// Gets a biography of the person.
        /// </summary>
        [JsonProperty("bio")]
        public string Bio { get; internal set; }

        /// <summary>
        /// Gets details of a photograph of the person on IGDB.
        /// </summary>
        [JsonProperty("person_mug_shot")]
        public ImageInfo Mugshot { get; internal set; }
    }
}
