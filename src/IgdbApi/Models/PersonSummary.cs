﻿namespace IgdbApi.Models
{
    /// <summary>
    /// Represents basic details of a person.
    /// </summary>
    public class PersonSummary : PersonBase
    {
    }
}
