﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace IgdbApi.Models
{
    /// <summary>
    /// Represents information about an image on IGDB.
    /// </summary>
    public class ImageInfo
    {
        /// <summary>
        /// Gets the ID of the image.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; internal set; }

        /// <summary>
        /// Gets the public URL of the image.
        /// </summary>
        [JsonProperty("url")]
        public Uri Url { get; internal set; }

        /// <summary>
        /// Gets the width of the image in pixels.
        /// </summary>
        [JsonProperty("width")]
        public int? Width { get; internal set; }

        /// <summary>
        /// Gets the height of the image in pixels.
        /// </summary>
        [JsonProperty("height")]
        public int? Height { get; internal set; }
    }
}
