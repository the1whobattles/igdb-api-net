﻿using Newtonsoft.Json;

namespace IgdbApi.Models
{
    /// <summary>
    /// Represents basic details of a company.
    /// </summary>
    public class CompanySummary : CompanyBase
    {
    }
}
