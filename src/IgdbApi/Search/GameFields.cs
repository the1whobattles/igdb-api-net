﻿namespace IgdbApi.Search
{
    /// <summary>
    /// Represents the field used for a game search.
    /// </summary>
    public enum GameFields
    {
        /// <summary>ID of the game.</summary>
        [SearchFilterQueryTerm("id")]
        Id,

        /// <summary>The URL slug of the game.</summary>
        [SearchFilterQueryTerm("slug")]
        Slug,

        /// <summary>Name of the game.</summary>
        [SearchFilterQueryTerm("name")]
        Name,

        /// <summary>Alternative names list of the game.</summary>
        [SearchFilterQueryTerm("alternative_name")]
        AlternativeName,

        /// <summary>Summary of the game.</summary>
        [SearchFilterQueryTerm("summary")]
        Summary,

        /// <summary>Storyline of the game.</summary>
        [SearchFilterQueryTerm("storyline")]
        Storyline,

        /// <summary>Rating of the game.</summary>
        [SearchFilterQueryTerm("rating")]
        Rating,

        /// <summary>Hypes that the game has.</summary>
        [SearchFilterQueryTerm("hypes")]
        Hypes,

        /// <summary>A list of theme IDs that the game has.</summary>
        [SearchFilterQueryTerm("themes")]
        Themes,

        /// <summary>A list of game mode IDs that the game has.</summary>
        [SearchFilterQueryTerm("game_modes")]
        GameModes,

        /// <summary>A list of platform IDs that the game has.</summary>
        [SearchFilterQueryTerm("platforms")]
        Platforms,

        /// <summary>A list of genre IDs that the game has.</summary>
        [SearchFilterQueryTerm("genres")]
        Genres,

        /// <summary>A list of keyword IDs that the game has.</summary>
        [SearchFilterQueryTerm("keywords")]
        Keywords,

        /// <summary>The first release date of the game.</summary>
        [SearchFilterQueryTerm("release_date")]
        ReleaseDate,

        /// <summary>A list of names of the companies related to the game.</summary>
        [SearchFilterQueryTerm("companies")]
        Companies,

        /// <summary>The list of company IDs for companies related to the game.</summary>
        [SearchFilterQueryTerm("company_ids")]
        CompanyIds,

        /// <summary>The IDs of the ESRB and PEGI ratings given to the game.</summary>
        [SearchFilterQueryTerm("ratings")]
        Ratings,

        /// <summary>A list of related character IDs.</summary>
        [SearchFilterQueryTerm("character")]
        Characters,

        /// <summary>A list of related person IDs.</summary>
        [SearchFilterQueryTerm("people")]
        People
    }
}