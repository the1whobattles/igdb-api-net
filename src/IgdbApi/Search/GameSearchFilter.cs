﻿namespace IgdbApi.Search
{
    /// <summary>
    /// Represents a filter for game searches.
    /// </summary>
    public class GameSearchFilter : SearchFilterBase<GameFields> { }
}
