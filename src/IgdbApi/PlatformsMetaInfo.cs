﻿namespace IgdbApi
{
    /// <summary>
    /// Represents meta information for the IGDB game platforms collection.
    /// </summary>
    public class PlatformsMetaInfo : MetaInfo
    {
    }
}
