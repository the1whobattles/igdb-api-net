﻿using Newtonsoft.Json;

namespace IgdbApi
{
    /// <summary>
    /// Represents the basic information available for all collections in IGDB.
    /// </summary>
    public class MetaInfo
    {
        /// <summary>
        /// Gets the number of objects in the requested collection.
        /// </summary>
        [JsonProperty("size")]
        public int Size { get; internal set; }
    }
}
